package com.appias

object Libraries {
    const val timber = "com.jakewharton.timber:timber:${version.timberVersion}"

    object core {
        const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${version.kotlinVersion}"
        const val androidx = "androidx.appcompat:appcompat:${version.androidX}"
        const val androidCorex = "androidx.core:core-ktx:${version.androidX}"
        const val crashlytics = "com.crashlytics.sdk.android:crashlytics:2.10.1"
    }

    object ui {
        const val constraintlayout = "androidx.constraintlayout:constraintlayout:1.1.3"
        const val recyclerView = "androidx.recyclerview:recyclerview:1.0.0"
    }

    object rx {
        const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${version.rxAndroid}"
        const val rxJava = "io.reactivex.rxjava2:rxjava:${version.rxJava}"
    }

    object moxy {
        const val moxyX = "tech.schoolhelper:moxy-x:${version.moxy}"
        const val moxyAndroidX = "tech.schoolhelper:moxy-x-androidx:${version.moxy}"
        const val moxyCompiler = "tech.schoolhelper:moxy-x-compiler:${version.moxy}"
    }

    object retrofit {
        const val retrofit = "com.squareup.retrofit2:retrofit:${version.retrofit}"
        const val gsonConverter = "com.squareup.retrofit2:converter-gson:${version.retrofit}"
        const val retrofitRx = "com.squareup.retrofit2:adapter-rxjava2:${version.retrofit}"
        const val okhttp = "com.squareup.okhttp3:okhttp:${version.okHttp}"
        const val logging = "com.squareup.okhttp3:logging-interceptor:${version.okHttp}"
    }

    object dagger {
        const val dagger = "com.google.dagger:dagger:${version.dagger}"
        const val daggerCompiler = "com.google.dagger:dagger-compiler:${version.dagger}"
    }

    object test {
        const val junit = "junit:junit:4.12"
        const val mockito = "org.mockito:mockito-core:2.22.0"
        const val espresso = "androidx.test.espresso:espresso-core:${version.espresso}"
        const val espressoContrib = "androidx.test.espresso:espresso-contrib:${version.espresso}"
        const val kakao = "com.agoda.kakao:kakao:2.2.0"
        const val rules = "androidx.test:rules:1.1.1"
    }

    object version {
        const val kotlinVersion = "1.3.50"
        const val androidX = "1.1.0"
        const val timberVersion = "4.7.1"

        const val rxAndroid = "2.1.1"
        const val rxJava = "2.2.14"

        const val moxy = "1.7.0"

        const val okHttp = "4.2.1"
        const val retrofit = "2.6.0"
        const val dagger = "2.25.2"

        const val espresso = "3.2.0"
    }
}

