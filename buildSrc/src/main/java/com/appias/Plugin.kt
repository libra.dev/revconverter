package com.appias

object Plugin {
    object ClassPaths {
        const val buildGradle = "com.android.tools.build:gradle:3.5.2"
        const val detekt = "io.gitlab.arturbosch.detekt:detekt-gradle-plugin:1.0.1"
        const val gsmServices = "com.google.gms:google-services:4.3.3"
        const val fabric = "io.fabric.tools:gradle:1.31.2"
        const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Libraries.version.kotlinVersion}"
    }
}