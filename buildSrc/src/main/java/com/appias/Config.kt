package com.appias

object Config {
    val buildToolsVersion = "29.0.2"
    val minSdkVersion = 21
    val targetSdkVersion = 28
    val compileSdkVersion = 28
}