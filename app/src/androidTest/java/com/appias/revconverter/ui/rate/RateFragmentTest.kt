package com.appias.revconverter.ui.rate

import android.content.pm.ActivityInfo
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.appias.revconverter.R
import com.appias.revconverter.data.model.RateItem
import com.appias.revconverter.ui.MainActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.math.BigDecimal

@RunWith(AndroidJUnit4::class)
class RateFragmentTest {

    @JvmField
    @Rule
    val rule = ActivityTestRule<MainActivity>(MainActivity::class.java)
    private var rateFragment: RateView = RateFragment()
    private val rateScreen = RateScreen()

    @Before
    fun init() {
        rateFragment = rule.activity.supportFragmentManager.fragments[0] as RateFragment
        initTest()
    }

    private fun initTest() {
        rateScreen {
            titleText {
                isDisplayed()
                hasText(R.string.rates)
            }
            pbLoading.isVisible()
            rvRates.isGone()
            llError.isGone()

            Thread.sleep(2000)
            rvRates.isVisible()
            llError.isGone()
            pbLoading.isGone()
        }
    }

    @Test
    fun testShowError() {
        rule.activity.runOnUiThread { rateFragment.showError() }
        rateScreen {
            Thread.sleep(200)
            llError.isVisible()
            rvRates.isGone()

            pbLoading.isGone()
            rateScreen.btnRetry.isVisible()
            rateScreen.btnRetry.click()
            pbLoading.isVisible()

            Thread.sleep(1500)
            rvRates.isVisible()
            pbLoading.isGone()
        }
    }

    @Test
    fun testSetRateData() {
        rule.activity.runOnUiThread { rateFragment.setRateData(arrayListOf(RateItem("EUR", BigDecimal(30.0), "🇪🇺"))) }
        Thread.sleep(100)
        assert(rateScreen.rvRates.getSize() == 1)
        rateScreen.pbLoading.isGone()
        rateScreen.llError.isGone()
        rateScreen.rvRates.isVisible()
    }

    @Test
    fun testList() {
        rateScreen {
            assert(rateScreen.rvRates.getSize() == 33)
            rvRates {
                isVisible()
                firstChild<Item> {
                    isVisible()
                    tvTitle.hasText("EUR")
                    tvSubtitle.hasText("EUR")
                    edValue.hasText("1.00")
                }
                Thread.sleep(1000)
                perform { childAt<Item>(3) { click() } }
                Thread.sleep(1000)
                firstChild<Item> {
                    isVisible()
                    tvTitle.hasText("MXN")
                    tvSubtitle.hasText("MXN")
                }
            }
        }
    }

    @Test
    fun testChangeOrientation() {
        rateScreen {
            assert(rateScreen.rvRates.getSize() == 33)
            rvRates {
                isVisible()
                firstChild<Item> {
                    isVisible()
                    tvTitle.hasText("EUR")
                    tvSubtitle.hasText("EUR")
                    edValue.hasText("1.00")
                }
                Thread.sleep(1000)
                perform { childAt<Item>(3) { click() } }
                Thread.sleep(1000)
                firstChild<Item> {
                    isVisible()
                    tvTitle.hasText("MXN")
                    tvSubtitle.hasText("MXN")
                }
                rule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                Thread.sleep(1000)
                firstChild<Item> {
                    isVisible()
                    tvTitle.hasText("MXN")
                    tvSubtitle.hasText("MXN")
                }
                perform { childAt<Item>(2) { click() } }
                Thread.sleep(1000)
                firstChild<Item> {
                    isVisible()
                    tvTitle.hasText("CHF")
                    tvSubtitle.hasText("CHF")
                }
                rule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                Thread.sleep(1000)
                firstChild<Item> {
                    isVisible()
                    tvTitle.hasText("CHF")
                    tvSubtitle.hasText("CHF")
                }
            }
        }
    }

    @Test
    fun testScrollAndSelect() {
        rateScreen.rvRates {
            isVisible()
            assert(rateScreen.rvRates.getSize() == 33)
            scrollTo(20)
            Thread.sleep(1000)
            perform {
                childAt<Item>(22) {
                    click()
                }
            }
            Thread.sleep(500)
            firstChild<Item> {
                isVisible()
                tvTitle.hasText("CAD")
                tvSubtitle.hasText("CAD")
            }
            perform {
                childAt<Item>(1) {
                    click()
                }
            }
            Thread.sleep(1500)
            firstChild<Item> {
                isVisible()
                tvTitle.hasText("EUR")
                tvSubtitle.hasText("EUR")
            }
        }
    }
}