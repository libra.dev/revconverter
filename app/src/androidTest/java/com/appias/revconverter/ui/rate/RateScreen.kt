package com.appias.revconverter.ui.rate

import android.view.View
import com.agoda.kakao.common.views.KView
import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.progress.KProgressBar
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KButton
import com.agoda.kakao.text.KTextView
import com.appias.revconverter.R
import org.hamcrest.Matcher

class RateScreen : Screen<RateScreen>() {

    val titleText = KTextView { withId(R.id.tvTitleRates) }
    val pbLoading = KProgressBar { withId(R.id.pbLoading) }
    val llError = KView { withId(R.id.llError) }
    val btnRetry = KButton { withId(R.id.btnRetry) }

    val rvRates = KRecyclerView({
        withId(R.id.rvRates)
    }, itemTypeBuilder = {
        itemType(::Item)
    })
}

class Item(parent: Matcher<View>) : KRecyclerItem<Item>(parent) {
    val tvTitle: KTextView = KTextView(parent) { withId(R.id.tvTitle) }
    val tvSubtitle: KTextView = KTextView(parent) { withId(R.id.tvSubtitle) }
    val tvFlag: KTextView = KTextView(parent) { withId(R.id.tvFlag) }
    val edValue: KEditText = KEditText(parent) { withId(R.id.edValue) }
}