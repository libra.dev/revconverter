package com.appias.revconverter.ui.rate

import com.appias.revconverter.data.model.RateItem
import com.appias.revconverter.data.repository.RateRepositoryStub
import com.appias.revconverter.interactor.rate.IRateInteractor
import com.appias.revconverter.interactor.rate.RateInteractor
import com.appias.revconverter.ui.rate.RatePresenter.Companion.BASE_DEFAULT
import com.appias.revconverter.ui.rate.RatePresenter.Companion.START_VALUE
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal

class RatePresenterTest {
    private val IRateRepository = RateRepositoryStub()
    private var iRateInteractor: IRateInteractor = RateInteractor(IRateRepository)
    private val ratePresenter = RatePresenter(iRateInteractor)
    private var rateListCache = arrayListOf<RateItem>()
    private var stubRateListCache = arrayListOf<RateItem>()

    @Before
    fun setUp() {
        iRateInteractor.loadRates(BASE_DEFAULT, BigDecimal(START_VALUE))
            .map { iRateInteractor.updateCacheList(it, rateListCache) }
            .subscribe { rateListCache = it }
        stubRateListCache.apply {
            add(RateItem("CNY", BigDecimal(11.1274), "11.12"))
            add(RateItem("INR", BigDecimal(17.4168), "17.41"))
            add(RateItem("MXN", BigDecimal(122.313), "122.31"))
            add(RateItem("ZAR", BigDecimal(117.782), "117.78"))
            add(RateItem("HRK", BigDecimal(183.523), "183.52"))
            add(RateItem("CHF", BigDecimal(117.9266), "117.92"))
        }
    }

    @Test
    fun setCurrentValue() {
        val testRateItem = RateItem(BASE_DEFAULT, BigDecimal(100.0), "100.0")
        rateListCache = ratePresenter.updateCurrentPosition(testRateItem)
        assert(rateListCache.first().value == testRateItem.value)
        assert(rateListCache.first().price == testRateItem.price)
        assert(rateListCache.first().name == testRateItem.name)
    }

    @Test
    fun prepareList() {
        rateListCache = iRateInteractor.updateCacheList(stubRateListCache, rateListCache)
        val size = stubRateListCache.count() - 1
        assert(rateListCache.size > stubRateListCache.size)
        assert(rateListCache[1].name == stubRateListCache.last().name)
        assert(rateListCache[1].value == stubRateListCache.last().value)
        assert(rateListCache[2].name == stubRateListCache[size - 1].name)
        assert(rateListCache[2].value == stubRateListCache[size - 1].value)
        assert(rateListCache[3].value == stubRateListCache[2].value)
        assert(rateListCache[3].name == stubRateListCache[2].name)
        assert(rateListCache[4].value == stubRateListCache[3].value)
        assert(rateListCache[4].name == stubRateListCache[3].name)
        assert(rateListCache[5].name == stubRateListCache[1].name)
        assert(rateListCache[5].value == stubRateListCache[1].value)
        assert(rateListCache[6].value == stubRateListCache.first().value)
        assert(rateListCache[6].value == stubRateListCache.first().value)
    }
}