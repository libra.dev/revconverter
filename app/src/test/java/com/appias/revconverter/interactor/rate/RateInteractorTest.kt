package com.appias.revconverter.interactor.rate

import com.appias.revconverter.data.model.RateItem
import com.appias.revconverter.data.repository.RateRepositoryStub
import com.appias.revconverter.extension.checkPattern
import com.appias.revconverter.ui.rate.RatePresenter.Companion.BASE_DEFAULT
import com.appias.revconverter.ui.rate.RatePresenter.Companion.START_VALUE
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.util.Locale

class RateInteractorTest {
    private var rateInteractor: RateInteractor? = null
    private var fakeList = arrayListOf<RateItem>()
    private var updateFakeList = arrayListOf<RateItem>()
    private var flagList = arrayListOf<String>()

    @Before
    fun setUp() {
        rateInteractor = RateInteractor(RateRepositoryStub())
        fakeList.apply {
            add(RateItem("CHF", BigDecimal(1.1274), "1.12"))
            add(RateItem("HRK", BigDecimal(7.4168), "7.41"))
            add(RateItem("MXN", BigDecimal(22.313), "22.31"))
            add(RateItem("ZAR", BigDecimal(17.782), "17.78"))
            add(RateItem("INR", BigDecimal(83.523), "83.52"))
            add(RateItem("CNY", BigDecimal(7.9266), "7.926"))
        }
        updateFakeList.apply {
            add(RateItem("CHF", BigDecimal(31.1274), "31.1274"))
            add(RateItem("MXN", BigDecimal(222.313), "222.313"))
            add(RateItem("CNY", BigDecimal(44.9266), "44.9266"))
            add(RateItem("ZAR", BigDecimal(317.782), "317.782"))
            add(RateItem("INR", BigDecimal(483.523), "483.523"))
            add(RateItem("HRK", BigDecimal(27.4168), "27.4168"))
            add(RateItem("EUR", BigDecimal(0.4168), "0.4168"))
        }
        flagList.apply {
            add("🇪🇺")
            add("🇨🇭")
            add("🇭🇷")
            add("🇲🇽")
            add("🇿🇦")
            add("🇮🇳")
            add("🇨🇳")
            add("🇹🇭")
            add("🇦🇺")
            add("🇮🇱")
            add("🇰🇷")
            add("🇯🇵")
            add("🇵🇱")
            add("🇬🇧")
            add("🇮🇩")
            add("🇭🇺")
            add("🇵🇭")
            add("🇹🇷")
            add("🇷🇺")
            add("🇭🇰")
            add("🇮🇸")
            add("🇩🇰")
            add("🇨🇦")
            add("🇲🇾")
            add("🇺🇸")
            add("🇧🇬")
            add("🇳🇴")
            add("🇷🇴")
            add("🇸🇬")
            add("🇨🇿")
            add("🇸🇪")
            add("🇳🇿")
            add("🇧🇷")
        }
    }

    @Test
    fun loadRates() {
        var i = 0
        rateInteractor!!.loadRates(BASE_DEFAULT, BigDecimal(START_VALUE))
            .map {
                assert(it.size > fakeList.size)
                assert(it.size == fakeList.size + 1)
                assert(it.first().name == BASE_DEFAULT)
                assert(it.first().value == BigDecimal(START_VALUE))
                it
            }
            .flatMapIterable { it }
            .map {
                assert(it.emoji!!.isNotEmpty())
                assert(it.emoji == flagList[i])
                i++
                it
            }
            .map {
                assert(it.price.isNotEmpty())
                assert(it.price.checkPattern())
            }
            .subscribe { }
    }

    @Test
    fun updatePricesTest() {
        val newValue = BigDecimal(22.0)
        rateInteractor?.updatePrices(fakeList, newValue)?.forEachIndexed { index, it ->
            val valuePrice = fakeList[index].value * newValue
            val valueString = String.format(Locale.US, "%.02f", valuePrice)
            assert(fakeList[index].price == it.price)
            assert(valueString == it.price)
        }
    }

    @Test
    fun updateCacheList() {
        assert(updateFakeList.size > fakeList.size)
        val size = fakeList.size
        rateInteractor!!.updateCacheList(updateFakeList, fakeList).forEachIndexed { index, item ->
            assert(fakeList.size == updateFakeList.size)
            assert(fakeList.size > size)
            updateFakeList.forEach { updateRateItem ->
                if (updateRateItem.name == item.name) {
                    assert(item.value == updateRateItem.value)
                    assert(item.price == updateRateItem.price)
                }
            }
        }
    }
}