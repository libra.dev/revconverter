package com.appias.revconverter.extension

import com.appias.revconverter.ui.rate.adapter.RateViewHolder.Companion.KEY_RESULT_SUCCESS
import org.junit.Test

class StringKtTest {

    @Test
    fun addInputMaskText() {
        assert("".addInputMaskText() == "0")
        assert("1".addInputMaskText() != null)
        assert("1.0.2".addInputMaskText() == "10.2")
        assert("122.022.222".addInputMaskText() == "122022.222")
        assert("022.22".addInputMaskText() == "22.22")
        assert("22.002342234234".addInputMaskText() == "22.00")
        assert("22.220".addInputMaskText() == "22.22")
        assert(".22".addInputMaskText() == "0.22")
        assert("1.23332".addInputMaskText() == "1.23")
        assert("22.22".addInputMaskText() == KEY_RESULT_SUCCESS)
    }

    @Test
    fun patternTest() {
        assert(!"".checkPattern())
        assert("2.3".checkPattern())
        assert("2".checkPattern())
        assert("2.44".checkPattern())
        assert("2.".checkPattern())
        assert("0.23".checkPattern())
        assert("123123120.23".checkPattern())
        assert("1234567890123456".checkPattern())
        assert("123456789012.00".checkPattern())
        assert(!"1234567890121123232".checkPattern())
        assert(!"2.233".checkPattern())
        assert(!"2.4444".checkPattern())
    }
}