package com.appias.revconverter.extension

import com.appias.revconverter.ui.rate.adapter.RateViewHolder
import com.appias.revconverter.ui.rate.adapter.RateViewHolder.Companion.KEY_RESULT_SUCCESS
import java.util.regex.Pattern

private const val zero = "0"
private const val format = "\\d{0,15}.\\d{0,2}"
private val pattern = Pattern.compile(format)

/**
Input mask for runtime input text:
- Filter value started with 0
- Filter value ended with more number than 2 after point
- Filter double points
 **/
fun String.addInputMaskText(): String? {
    if (isEmpty()) return zero
    if (startsWith(zero) && this.length > 1) {
        return removePrefix(zero)
    }
    val arrayString = split(RateViewHolder.POINT)
    if (arrayString.size > 2) {
        val indexOfPoint = indexOf(RateViewHolder.POINT)
        return removeRange(indexOfPoint, indexOfPoint + 1)
    }
    if (arrayString.size == 2 && arrayString[1].length > 2) {
        val remove = removeRange(arrayString[0].length + 3, length)
        if (startsWith(RateViewHolder.POINT)) {
            return zero + remove
        }
        if (endsWith(RateViewHolder.POINT)) {
            return remove + zero
        }
        return remove
    }
    if (startsWith(RateViewHolder.POINT)) {
        return zero + this
    }
    return KEY_RESULT_SUCCESS
}

fun String.checkPattern(): Boolean {
    return pattern.matcher(this).matches()
}