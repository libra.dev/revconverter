package com.appias.revconverter

import android.app.Application
import com.appias.revconverter.di.AppComponent
import com.appias.revconverter.di.AppModule
import com.appias.revconverter.di.DaggerAppComponent
import timber.log.Timber

class RevConverterApp : Application() {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        if (isTestBuild) {
            Timber.plant(Timber.DebugTree())
        }
        appComponent = buildComponent().build()
    }

    fun getAppComponent(): AppComponent = appComponent

    private fun buildComponent(): DaggerAppComponent.Builder {
        return DaggerAppComponent.builder()
            .appModule(AppModule(this))
    }

    companion object {
        private const val RELEASE_BUILD_TYPE = "release"

        val isTestBuild: Boolean
            get() = BuildConfig.BUILD_TYPE != RELEASE_BUILD_TYPE
    }
}