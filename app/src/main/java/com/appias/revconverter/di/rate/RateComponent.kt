package com.appias.revconverter.di.rate

import com.appias.revconverter.ui.rate.RateFragment
import dagger.Subcomponent

@Subcomponent(modules = [RateModule::class])
interface RateComponent {
    fun plus(rateFragment: RateFragment)
}