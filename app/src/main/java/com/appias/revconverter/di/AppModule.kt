package com.appias.revconverter.di

import android.content.Context
import com.appias.revconverter.BuildConfig
import com.appias.revconverter.RevConverterApp
import com.appias.revconverter.data.repository.api.RateApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

@Module
class AppModule(private val app: RevConverterApp) {

    @Provides
    @Singleton
    fun getContext(): Context = app

    @Provides
    @Singleton
    fun getConverterApi(okHttpClient: OkHttpClient, gson: Gson): RateApi {
        val rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(rxAdapter)
            .build()
            .create(RateApi::class.java)
    }

    @Provides
    @Singleton
    fun getOkHttp(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }
        return builder.build()
    }

    @Provides
    @Singleton
    fun getGson(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSS'Z'")
            .create()
    }
}