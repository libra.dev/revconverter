package com.appias.revconverter.di.rate

import com.appias.revconverter.data.repository.IRateRepository
import com.appias.revconverter.data.repository.RateRepository
import com.appias.revconverter.data.repository.api.RateApi
import com.appias.revconverter.interactor.rate.IRateInteractor
import com.appias.revconverter.interactor.rate.RateInteractor
import com.appias.revconverter.ui.rate.RatePresenter
import dagger.Module
import dagger.Provides

@Module
class RateModule {

    @Provides
    fun getRateRepository(rateApi: RateApi): IRateRepository {
        return RateRepository(rateApi)
    }

    @Provides
    fun getRateInteractor(iRateRepository: IRateRepository): IRateInteractor {
        return RateInteractor(iRateRepository)
    }

    @Provides
    fun getRatePresenter(iRateInteractor: IRateInteractor): RatePresenter {
        return RatePresenter(iRateInteractor)
    }
}