package com.appias.revconverter.di

import com.appias.revconverter.di.rate.RateComponent
import com.appias.revconverter.di.rate.RateModule
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface AppComponent {
    fun inject(rateModule: RateModule): RateComponent
}