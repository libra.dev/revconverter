package com.appias.revconverter.interactor.rate

import com.appias.revconverter.data.model.RateItem
import com.appias.revconverter.data.repository.IRateRepository
import io.reactivex.Observable
import java.math.BigDecimal
import java.util.Locale
import kotlin.collections.ArrayList

private const val VALUE_FORMAT = "%.02f"

class RateInteractor(private val iRateRepository: IRateRepository) : IRateInteractor {

    override fun loadRates(base: String, currentValue: BigDecimal): Observable<MutableList<RateItem>> {
        return iRateRepository.loadRates(base)
            .map { it.rates.toList() }
            .flatMapIterable { it }
            .map { RateItem(it.first, it.second, preparePrice(it.second, currentValue), localeToEmoji(it.first)) }
            .toList()
            .map {
                it.add(0, RateItem(base, currentValue, preparePrice(currentValue), localeToEmoji(base)))
                return@map it
            }
            .toObservable()
    }

    private fun localeToEmoji(countryCode: String): String {
        val firstLetter = Character.codePointAt(countryCode, 0) - 0x41 + 0x1F1E6
        val secondLetter = Character.codePointAt(countryCode, 1) - 0x41 + 0x1F1E6
        return String(Character.toChars(firstLetter)) + String(Character.toChars(secondLetter))
    }

    private fun preparePrice(value: BigDecimal, currentValue: BigDecimal = BigDecimal(1.0)): String {
        if (value == currentValue) return String.format(Locale.US, VALUE_FORMAT, value)
        val measurePrice = value * currentValue
        return String.format(Locale.US, VALUE_FORMAT, measurePrice)
    }

    override fun updateCacheList(
        updateRateList: MutableList<RateItem>,
        rateListCache: ArrayList<RateItem>
    ): ArrayList<RateItem> {
        updateRateList.forEach { rateItem ->
            if (rateListCache.isNotEmpty() && rateListCache.map { it.name }.contains(rateItem.name)) {
                val index = rateListCache.map { it.name }.indexOf(rateItem.name)
                if (index != -1) {
                    rateListCache[index].value = rateItem.value
                    rateListCache[index].price = rateItem.price
                }
            } else {
                rateListCache.add(rateItem)
            }
        }
        return rateListCache
    }

    override fun updatePrices(
        rateListCache: ArrayList<RateItem>,
        updateCurrentValue: BigDecimal
    ): ArrayList<RateItem> {
        rateListCache.forEach {
            it.price = preparePrice(it.value, updateCurrentValue)
        }
        return rateListCache
    }
}