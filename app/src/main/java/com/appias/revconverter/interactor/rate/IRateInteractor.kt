package com.appias.revconverter.interactor.rate

import com.appias.revconverter.data.model.RateItem
import com.appias.revconverter.ui.rate.RatePresenter.Companion.BASE_DEFAULT
import io.reactivex.Observable
import java.math.BigDecimal

interface IRateInteractor {
    fun loadRates(base: String = BASE_DEFAULT, currentValue: BigDecimal): Observable<MutableList<RateItem>>

    fun updateCacheList(updateRateList: MutableList<RateItem>, rateListCache: ArrayList<RateItem>): ArrayList<RateItem>

    fun updatePrices(rateListCache: ArrayList<RateItem>, updateCurrentValue: BigDecimal): ArrayList<RateItem>
}