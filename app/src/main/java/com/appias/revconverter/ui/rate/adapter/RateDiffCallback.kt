package com.appias.revconverter.ui.rate.adapter

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.appias.revconverter.data.model.RateItem

class RateDiffCallback(
    private val newList: List<RateItem>,
    private val oldList: ArrayList<RateItem>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].price == newList[newItemPosition].price &&
                oldList[oldItemPosition].value == newList[newItemPosition].value &&
                oldList[oldItemPosition].name == newList[newItemPosition].name
    }

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        if (oldItemPosition == 0 && newItemPosition == 0) return true
        return false
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val newContact = newList[newItemPosition]
        val oldContact = oldList[oldItemPosition]

        val diff = Bundle()
        if (newContact.name != oldContact.name) {
            diff.putString("name", newContact.name)
        }
        if (newContact.price != oldContact.price) {
            diff.putString("price", newContact.price)
        }
        return if (diff.size() == 0) Any() else diff
    }
}