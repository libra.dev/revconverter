package com.appias.revconverter.ui.rate

import com.appias.revconverter.data.model.RateItem
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface RateView : MvpView {
    fun setRateData(rates: ArrayList<RateItem>)
    fun showError()
}