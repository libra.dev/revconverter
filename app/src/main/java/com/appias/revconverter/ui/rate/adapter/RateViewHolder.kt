package com.appias.revconverter.ui.rate.adapter

import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.appias.revconverter.data.model.RateItem
import com.appias.revconverter.extension.addInputMaskText
import com.appias.revconverter.extension.checkPattern
import com.appias.revconverter.extension.showKeyboard
import kotlinx.android.synthetic.main.item_rate.view.*
import java.math.BigDecimal

class RateViewHolder(
    view: View,
    private var onItemListener: ((RateItem) -> Unit)?,
    private var onBaseValueListener: ((BigDecimal) -> Unit)?
) : RecyclerView.ViewHolder(view), TextWatcher {

    fun bind(rateItem: RateItem, position: Int) {
        itemView.apply {
            tvTitle.text = rateItem.name
            tvSubtitle.text = rateItem.name
            tvFlag.text = rateItem.emoji
            setOnClickListener { choseItem(position, rateItem, edValue) }
            setEditTextParameters(position, rateItem, edValue)
        }
    }

    private fun choseItem(position: Int, rateItem: RateItem, edValue: EditText) {
        if (position == 0) {
            itemView.context.showKeyboard(edValue)
            return
        }
        onItemListener?.invoke(rateItem)
    }

    private fun setEditTextParameters(position: Int, rateItem: RateItem, edValue: EditText) {
        edValue.apply {
            if (position == 0) {
                isEnabled = true
                if (!isFocused) {
                    setText(rateItem.price)
                }
                addTextChangedListener(this@RateViewHolder)
                filters = firstItemFilter
            } else {
                filters = allItemsFilter
                isEnabled = false
                setText(rateItem.price)
            }
        }
    }

    override fun afterTextChanged(s: Editable?) {
        if (itemView.edValue.isFocused) {
            val valueString = s.toString()
            val result = valueString.addInputMaskText()
            if (!result.isNullOrEmpty() && result != KEY_RESULT_SUCCESS) {
                itemView.edValue.apply {
                    removeTextChangedListener(this@RateViewHolder)
                    setText(result)
                    onCheckValue(result)
                    addTextChangedListener(this@RateViewHolder)
                }
            }
            onCheckValue(valueString)
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    private fun onCheckValue(valueString: String) {
        if (valueString.checkPattern()) {
            val value = valueString.toDoubleOrNull() ?: 0.0
            onBaseValueListener?.invoke(BigDecimal(value))
        }
    }

    companion object {
        private val firstItemFilter = arrayOf<InputFilter>(InputFilter.LengthFilter(15))
        private val allItemsFilter = arrayOf<InputFilter>(InputFilter.LengthFilter(20))
        const val POINT = "."
        const val KEY_RESULT_SUCCESS = "KEY_RESULT_SUCCESS"
    }
}