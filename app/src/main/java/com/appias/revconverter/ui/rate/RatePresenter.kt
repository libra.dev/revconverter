package com.appias.revconverter.ui.rate

import com.appias.revconverter.data.model.RateItem
import com.appias.revconverter.interactor.rate.IRateInteractor
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.crashlytics.android.Crashlytics
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

@InjectViewState
class RatePresenter(private val iRateInteractor: IRateInteractor) : MvpPresenter<RateView>() {

    private var loadDisposable: Disposable? = null
    private var updateDisposable: Disposable? = null
    private var currentBase = BASE_DEFAULT
    private var currentValue = BigDecimal(START_VALUE)
    private var rateListCache = arrayListOf<RateItem>()

    override fun attachView(view: RateView?) {
        super.attachView(view)
        loadRate(currentBase)
    }

    override fun detachView(view: RateView?) {
        super.detachView(view)
        unsubscribe()
    }

    fun setCurrentValue(updateCurrentValue: BigDecimal) {
        currentValue = updateCurrentValue
        unsubscribeUpdate()
        updateDisposable = Observable.just(updateCurrentValue)
            .map { iRateInteractor.updatePrices(rateListCache, it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.setRateData(it)
            }, { handleError(it) })
    }

    fun updateCurrentPosition(rateItem: RateItem): ArrayList<RateItem> {
        rateListCache.remove(rateItem)
        rateListCache.add(0, rateItem)
        viewState.setRateData(rateListCache)
        currentValue = BigDecimal(rateItem.price.toDouble())
        currentBase = rateItem.name
        return rateListCache
    }

    fun loadRate(base: String = currentBase, value: BigDecimal = currentValue) {
        currentValue = value
        currentBase = base
        unsubscribeLoad()
        loadDisposable = Observable.interval(TIME_REPEAT, TimeUnit.MILLISECONDS)
            .flatMap { iRateInteractor.loadRates(currentBase, currentValue) }
            .map { iRateInteractor.updateCacheList(it, rateListCache) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.setRateData(it)
            }, { handleError(it) })
    }

    fun unsubscribe() {
        unsubscribeLoad()
        unsubscribeUpdate()
    }

    private fun handleError(it: Throwable) {
        Timber.e("Error: $it")
        Crashlytics.logException(it)
        viewState.showError()
    }

    private fun unsubscribeLoad() {
        loadDisposable?.let {
            it.dispose()
            loadDisposable = null
        }
    }

    private fun unsubscribeUpdate() {
        updateDisposable?.let {
            it.dispose()
            updateDisposable = null
        }
    }

    companion object {
        private const val TIME_REPEAT = 1000L
        const val BASE_DEFAULT = "EUR"
        const val START_VALUE = 1.0
    }
}