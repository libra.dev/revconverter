package com.appias.revconverter.ui.rate.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appias.revconverter.R
import com.appias.revconverter.data.model.RateItem
import androidx.recyclerview.widget.DiffUtil
import java.math.BigDecimal

class RateAdapter : RecyclerView.Adapter<RateViewHolder>() {
    private var rateList = arrayListOf<RateItem>()
    private var onItemListener: ((RateItem) -> Unit)? = null
    private var onBaseValueListener: ((BigDecimal) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_rate, parent, false)
        return RateViewHolder(view, onItemListener, onBaseValueListener)
    }

    override fun getItemCount(): Int = rateList.size

    override fun onBindViewHolder(holder: RateViewHolder, position: Int) {
        holder.bind(rateList[position], position)
    }

    fun setData(data: ArrayList<RateItem>) {
        val diffResult = DiffUtil.calculateDiff(RateDiffCallback(data, rateList), false)
        rateList.clear()
        rateList.addAll(data)
        diffResult.dispatchUpdatesTo(this)
    }

    fun setOnItemClickListener(callBack: ((RateItem) -> Unit)) {
        onItemListener = callBack
    }

    fun setOnBaseValueChangeListener(callBack: ((BigDecimal) -> Unit)) {
        onBaseValueListener = callBack
    }
}