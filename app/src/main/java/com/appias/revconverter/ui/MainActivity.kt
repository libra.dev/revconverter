package com.appias.revconverter.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.appias.revconverter.R
import com.appias.revconverter.ui.rate.RateFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setTheme(R.style.AppTheme)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.frContainer, RateFragment.getFragment()).commit()
        }
    }
}
