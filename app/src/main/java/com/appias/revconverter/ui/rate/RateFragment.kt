package com.appias.revconverter.ui.rate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.appias.revconverter.R
import com.appias.revconverter.RevConverterApp
import com.appias.revconverter.data.model.RateItem
import com.appias.revconverter.di.rate.RateModule
import com.appias.revconverter.ui.rate.adapter.RateAdapter
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fr_rates.*
import javax.inject.Inject

class RateFragment : MvpAppCompatFragment(), RateView {

    @Inject
    @InjectPresenter
    lateinit var ratePresenter: RatePresenter
    private val rateAdapter = RateAdapter()

    @ProvidePresenter
    fun providePresenter(): RatePresenter {
        (activity!!.applicationContext as RevConverterApp).getAppComponent().inject(RateModule()).plus(this)
        return ratePresenter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fr_rates, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvRates.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = rateAdapter
        }
        btnRetry.setOnClickListener {
            pbLoading.visibility = View.VISIBLE
            llError.visibility = View.GONE
            ratePresenter.loadRate()
        }
        rateAdapter.apply {
            setOnItemClickListener { rateItem ->
                ratePresenter.updateCurrentPosition(rateItem)
                rvRates.scrollToPosition(0)
            }
            setOnBaseValueChangeListener { ratePresenter.setCurrentValue(it) }
        }
    }

    override fun setRateData(rates: ArrayList<RateItem>) {
        rateAdapter.setData(rates)
        rvRates.visibility = View.VISIBLE
        llError.visibility = View.GONE
        pbLoading.visibility = View.GONE
    }

    override fun showError() {
        pbLoading.visibility = View.GONE
        rvRates.visibility = View.GONE
        llError.visibility = View.VISIBLE
        ratePresenter.unsubscribe()
    }

    companion object {
        fun getFragment() = RateFragment()
    }
}