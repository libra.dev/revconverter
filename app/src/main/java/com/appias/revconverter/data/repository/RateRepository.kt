package com.appias.revconverter.data.repository

import com.appias.revconverter.data.model.BaseRate
import com.appias.revconverter.data.repository.api.RateApi
import io.reactivex.Observable

class RateRepository(private val rateApi: RateApi) : IRateRepository {
    override fun loadRates(base: String): Observable<BaseRate> {
        return rateApi.loadRate(base)
    }
}