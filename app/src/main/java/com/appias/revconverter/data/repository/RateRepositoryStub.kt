package com.appias.revconverter.data.repository

import com.appias.revconverter.data.model.BaseRate
import io.reactivex.Observable
import java.math.BigDecimal

class RateRepositoryStub : IRateRepository {
    private var stubRates = hashMapOf<String, BigDecimal>()
    override fun loadRates(base: String): Observable<BaseRate> {
        stubRates.apply {
            put("CHF", BigDecimal(1.1274))
            put("HRK", BigDecimal(7.4168))
            put("MXN", BigDecimal(22.313))
            put("ZAR", BigDecimal(17.782))
            put("INR", BigDecimal(83.523))
            put("CNY", BigDecimal(7.9266))
        }
        return Observable.just(BaseRate(base, "2018-09-06", stubRates))
    }
}