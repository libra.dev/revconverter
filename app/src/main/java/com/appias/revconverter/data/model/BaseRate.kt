package com.appias.revconverter.data.model

import java.math.BigDecimal

data class BaseRate(
    val base: String,
    val date: String,
    val rates: HashMap<String, BigDecimal>
)