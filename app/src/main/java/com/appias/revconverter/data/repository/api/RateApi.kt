package com.appias.revconverter.data.repository.api

import com.appias.revconverter.data.model.BaseRate
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RateApi {
    @GET("latest")
    fun loadRate(@Query("base") base: String): Observable<BaseRate>
}