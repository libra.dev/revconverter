package com.appias.revconverter.data.repository

import com.appias.revconverter.data.model.BaseRate
import io.reactivex.Observable

interface IRateRepository {
    fun loadRates(base: String): Observable<BaseRate>
}