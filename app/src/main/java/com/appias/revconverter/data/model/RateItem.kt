package com.appias.revconverter.data.model

import java.math.BigDecimal

data class RateItem(
    val name: String,
    var value: BigDecimal,
    var price: String,
    var emoji: String? = null
)